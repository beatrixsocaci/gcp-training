def gen_tuples(start, end):
    return [(y,x) for x in range(start, end+1) for y in range(x, 0, -1)]
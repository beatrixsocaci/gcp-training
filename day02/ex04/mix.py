def mix_lists(list1, list2, list3, list4):
    return [{x: {'key_'+str(n): y}  for x in list1 for n in range(1, 5, 1) for y in zip(list2,list3,list4) }]
    


print(mix_lists(["my_key_name", 3], ["green", "brown", "blue"], [1, 2, 3, 4], ["string", "float", -1, 0]))
#list1 = ["my_key_name", 3]
#list2 = ["green", "brown", "blue"]
#list3 = [1, 2, 3, 4]
#list4 = ["string", "float", -1, 0]

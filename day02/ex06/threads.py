import _thread
import time

def create_threads():
    try:
        count = 1
        while count < 6:
            _thread.start_new_thread()
            time.sleep(count)
    except:
        print("Error:unable to start thread")

create_threads()
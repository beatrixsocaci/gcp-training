top command is used to show the Linux processes. It provides a dynamix real-time view of the running system. Usually, this command shows the summary information of the system and the list of processes or threads which are currently managed by the Linux Kernel. First column PID shows task's unique process id. The second column PR stands for priority of the task. SHR represents the amount of shared menory used by a task. VIRT is total virtual memory used by the task. USER is the user name of owner of task. %CPU represents the CPU usage. TIME+: is CPU Time, the same as 'TIME', but reflecting more granularity through hundredths of a second. SHR represents the Shared Memory size (kb) used by a task. NI represents a nice value of task. A negative nice value implies higher priority, and positive Nice value means lower priority. %MEM shows the memory usage of task.
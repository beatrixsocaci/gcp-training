def summary_dict(string_data):
    return [{"sender": x, "receiver": y, "message" : z } for x in string_data.split("sender") for y in string_data.split("receiver") for z in string_data.split("")]
summary_dict('id_00 sender: Maria message:"hello Gina, How are you?" receiver: Gina')
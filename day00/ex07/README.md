HTTP Head request the server only returns the headers without a body. It is much faster then the GET method because much less data is transfered in HEAD requests. 
The HTTP HEAD request is often used to retreve meta-information about resource at a specific URI without transmitting acutal data.

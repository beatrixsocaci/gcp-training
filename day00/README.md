Every file and directory has assigned 3 types of owners: user (the person who created the file), 
group (people who have acces to the file) and others (any person who is neither the owner neither in a group that has acces to the file) 
Permissions are clustered in: reading, writing (editing) and execute. In order to change permissions we can use the chmod command followed by
u=r/w/x(single or combined),g=r/w/x,o=r/w/x and the fileName, or we can simply write chmod and a number. This number is obtained by adding permissions 
values (read has value 4, write has 2, and execute has 1) so for granting permissions to everyoane we can say chmod 777, to restrain all permissions from a group or other people we say chmod 700 fileName.

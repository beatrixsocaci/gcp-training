def count_words():
	try:
		inp = input()
		list_of_words = inp.split( )
		number_of_words = len(list_of_words)
		if inp:
			return number_of_words
		if not inp:
			raise EOFError('No input provided')
	except EOFError as e:
		print(e)
nr = count_words()
if type(nr) == int:
	print("Number of words: %d" %nr)

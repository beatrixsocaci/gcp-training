import sys
from datetime import datetime

def get_details():
	var = str(sys.version)
	print("Python " + var[:6])
	now = datetime.now()
	dt_string = now.strftime("%d-%m-%Y %H:%M:%S")
	print("Current date and time: ", dt_string)
get_details()

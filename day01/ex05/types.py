def create_types():
    try:
        with open('demo.txt', 'r') as file:
            data = file.read().replace('\n', ',')
            if len(data) == 0:
                raise EOFError('File is empty')
            list_b = data.split(',')
            list_a =[]
            for i in range(len(list_b)):
                list_a.append(int(list_b[i]))
            tuple_a = tuple(list_a)
            dict_a = dict()
            for index, value in enumerate(list_a):
                dict_a[index] = value
            print(list_a)
            print(tuple_a)
            print(dict_a)        
    except (EOFError, FileNotFoundError, ValueError):
        print('Invalid input ')
create_types()

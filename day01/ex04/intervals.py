def get_interval():
	try:
		inp = input()
		inputs = inp.split()
		cond = True
		if not inp:
			cond = False
			raise EOFError('No input provided')
		if len(inputs) !=  2 and len(inputs) != 0:
			cond = False
			raise ValueError('invalid interval')
#		if type(inputs[0]) != int or type(inputs[0]) != float or type(inputs[1]) != int or type(inputs[1] != float:
		if inputs[0].isnumeric() == False or inputs[1].isnumeric() == False:
			cond = False
			raise TypeError('Incorrect type')
		if cond:
			a = int(inputs[0])
			b = int(inputs[1])
			if a < b:
				x = range(a, b+1)
				for num in x:
					print("%d " %num, end='')
			if a > b:
				x = range(b, a+1)
				for num in x:
					print("%d " %num, end='')
	except (ValueError, TypeError, EOFError) as e:
		print(e) 
get_interval()

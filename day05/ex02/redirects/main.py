from flask import Flask, render_template

app = Flask(__name__, template_folder='app/templates/public', static_folder='app/static')

@app.route('/index.html')
def hello():
    return render_template('index.html')

@app.route('/')
def first():
    return render_template('index.html')

@app.route('/prices.html')
def price():
    return render_template('prices.html')

@app.route('/contact.html')
def contact():
    return render_template('contact.html')
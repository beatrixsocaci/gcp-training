from google.cloud import datastore
datastore_client = datastore.Client()
kind = "name"
name="item_name"
item_key = datastore_client.key(kind,name)
item = datastore.Entity(key=item_key)
item["price"] = "6"
datastore_client.put(item)

print(f"Saved {item.key.name}: {item['price']}")
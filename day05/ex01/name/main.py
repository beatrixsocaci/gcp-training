from flask import Flask, render_template

app = Flask(__name__, template_folder='app/templates/public', static_folder='app/static')


@app.route('/hello_to_training/<name>')
def hello(name=None):
     return render_template('ex01.html', name=name)

if __name__ == '__main__':
    app.run(host="localhost", port=8080, debug=True)
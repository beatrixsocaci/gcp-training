from flask import Flask, render_template

app = Flask(__name__, template_folder='app/templates/public', static_folder='app/static')

@app.route('/hello_to_training')
def hello():
    return render_template('ex00.html')